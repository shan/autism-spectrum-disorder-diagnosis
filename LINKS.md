# Useful links

### Neural Images
- [Scikit SVM tutorial](https://machinelearninggeek.com/support-vector-machine-classification-in-scikit-learn/ "Still need to apply k-fold cv")
- [Holy Grail slides on ML 4 fMRIs](https://natmeg.se/onewebmedia/Alexandre%20Gramfort%20-%20Machine%20learning%20-%20Karolinska.pdf)

### Tutorials
- [Preprocessing](https://andysbrainbook.readthedocs.io/en/latest/ML/ML_Short_Course/ML_03_Haxby_Preprocessing.html)
- [Python for fMRI analysis](https://lukas-snoek.com/NI-edu/fMRI-introduction/week_1/python_for_mri.html)
- [Brain encoding and decoding in fMRI](https://main-educational.github.io/brain_encoding_decoding/haxby_data.html#preparing-the-fmri-data)
- [ML classifiers and fMRI](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC2892746/)

### References for the report
- [Does rs-fMRI preprocessing matter for prediction performance in machine learning?](https://school.brainhackmtl.org/project/rs-fmri-preprocessing/)

### Preprocessing software
- [~~BIDS directory format standard~~](https://www.nipreps.org/apps/framework/)
- [~~fmriprep~~](https://fmriprep.org/en/latest/index.html)
- [CPAC preprocessor](https://fcp-indi.github.io/docs/latest/user/quick.html) == same used for ABIDE I preprocessed

### Handling "Big Data"

- [DASK for scalable Scikit-learn](https://ml.dask.org/)
- [Scikit Incremental Learning guide](https://coderzcolumn.com/tutorials/machine-learning/scikit-learn-incremental-learning-for-large-datasets#2)

### CPAC reference
- [groups](https://groups.google.com/g/cpax_forum/search?q=preprocessing)
